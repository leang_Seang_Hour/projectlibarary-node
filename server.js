const connectDB = require('./DB/connectdb');
const mongoose = require('mongoose');

const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();

connectDB();
mongoose.Promise = global.Promise;
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.use(bodyParser.json({ limit: '10mb', extended: true }))
app.use(bodyParser.urlencoded({ limit: '10mb', extended: true }))
app.use(bodyParser.urlencoded({ extended: true }))
app.use(cors({ origin: 'http://localhost:4200' }));

app.use('/', require('./Route/costumer'));

app.listen(3000, () => {
    console.log("Express server start ar port 3000");
});