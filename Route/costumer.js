const express = require('express');
const mongoose = require('mongoose');
const Costumer = require('../Model/Costumer');
const route = express.Router();
const { ObjectID } = require('mongodb');
//Input & Insert Data in momgo local
route.post('/', async(req, res) => {
    console.log(req.body);
    if (!req.body.firstname) {
        console.log("this all field is require")
    } else {
        let costumer = {
            firstname: req.body.firstname,
            lastname: req.body.lastname,
            gender: req.body.gender,
            telephone: req.body.telephone,
            address: req.body.address,
            email: req.body.email,
            password: req.body.password,
        }
        let moduleUser = new Costumer(costumer);
        await moduleUser.save((err, doc) => {
            if (!err) {
                console.log(costumer)
                res.json(doc);
            } else {
                console.error('Error Insert category:' + JSON.stringify(err, undefined, 2));
            }
        })
    }

});
//Get All DATA from DB
route.get('/', async(req, res) => {
        Costumer.find((err, docs) => {
            if (!err) {
                res.send(docs);
            } else {
                console.err('error get all User from database' + JSON.stringify(err, undefined, 2));
            }
        });
    })
    //get 1 user
route.get('/:id', async(req, res) => {
        if (!ObjectID.isValid(req.params.id)) {
            return res.status(400).send(`this ID is not found in DB: ${req.params.id}`)
        }
        Costumer.findById(req.params.id, (err, doc) => {
            if (!err) {
                res.send(doc);
            } else {
                console.log('Error  get one category from the database: ' + JSON.stringify(err, undefined, 2));
            }
        })
    })
    //Update
route.put('/:id', async(req, res) => {
    if (!ObjectID.isValid(req.params.id)) {
        return res.status(400).send(`this id is not find into database : ${req.params.id}`);
    }
    //where we change our data when update
    console.log(req.body);
    let costumer = {
        lastname: req.body.lastname,
        firstname: req.body.firstname,
        gender: req.body.gender,
        telephone: req.body.telephone,
        address: req.body.address,
        email: req.body.email,
        password: req.body.password
    }
    Costumer.findByIdAndUpdate(req.params.id, { $set: costumer }, { new: true }, (err, doc) => {
        if (!err) {
            res.send(doc);
        } else {
            console.log('Error update category into database:' + JSON.stringify(err, undefined, 2));
        }
    })
})

module.exports = route;