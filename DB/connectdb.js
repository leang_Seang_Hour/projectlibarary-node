const mongoose = require('mongoose');
const URI = "mongodb+srv://admin:admin@cluster0.fezsj.mongodb.net/liblarydb?retryWrites=true&w=majority";



mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);
mongoose.set('useUnifiedTopology', true);
const connectDB = async() => {
    await mongoose.connect(URI, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });
    console.log("db connected");
}

module.exports = connectDB;